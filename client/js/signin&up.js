var app = angular.module('myApp', []);
app.controller('DataControler', function($scope, $http) {

    var users = []; // Get all user to validate
    var different = true; 
    var uid = 0;
    var check = true;
    var validid = 0;
    
    // Get all user 
    var url = 'http://nsp1010.herokuapp.com/users/';
    $http.get(url)
        .then(function(response) {
            users = response.data;
            uid = users.length;
            console.log(response.data);
        });



    // Check the username 
    var chkusername = document.getElementById('username').onchange = function() {
        var username = document.getElementById('username').value;
        if (users.length == 0) {
            different = true;
        }
        else {
            for (var i = 0; i < users.length; i++) {
                if (users[i].username == username) {
                    alert("This username has already used!");
                    different = false;
                }
                else {
                    different = true;
                }
            }
        }
        return different;
    };

    // Check the name
    var chkname = document.getElementById('name').onchange = function() {
        var name = document.getElementById('name').value;
        if (!name || (name.length < 3) || (name.length > 100) ||
            (name.indexOf(' ') == -1)) {
            alert('Please enter a valid full name!');
            check = false;
        }else{
            check = true;
        }
    };

    // Check the email
    var chkemail = document.getElementById('email').onchange = function() {
        var email = document.getElementById('email').value;
        if (!email || (email.length < 6) || (email.indexOf('@') == -1)) {
            alert('Please enter a valid email address!');
            check = false;
        }else{
            check = true;
        }
    };

    // Check the password
    var chkpassword = document.getElementById('password').onchange = function() {
        var password = document.getElementById('password').value;
        if (!password || (password.length < 1)) {
            alert('Please enter a password!');
            check = false;
        }else{
            check = true;
        }
    };

    // Sign Up
    document.getElementById('signUp').onclick = function() {
        // Check whether all input fields are validated or not
        if (chkusername() && check) {
            // Get all the valid input data
            var username = document.getElementById('username').value;
            var name = document.getElementById('name').value;
            var email = document.getElementById('email').value;
            var password = document.getElementById('password').value;
    
            // Add user 
            var newUser = {
                age:0,
                uid: uid,
                username: username,
                name: name,
                email: email,
                favour : [ ["Empty"] ],
                password: password
            };
            $http.post(url, newUser).then(function(response) {
                console.log(response.data);
            });
            uid += 1;
            alert("Sign up succussful!");
            window.location.reload(true); 
        }
        else {
            alert("Sign up fail, please check your input!");
        }
    };

    // Sign in
    document.getElementById('signIn').onclick = function() {
        var uname = document.getElementById('uname').value;
        var pwd = document.getElementById('pwd').value;

        if (validSignIn(uname, pwd)) {
            // If the username and password are matched, set it to session storage
            alert("Hello " + uname);
            sessionStorage.setItem("username", uname);
            sessionStorage.setItem("password", pwd);
            sessionStorage.setItem("uid", validid);
            // redirect to index page
            window.location.href = "../client/index.html";
        }
        else {
            alert("Sign in fail, please check your input!!!");
        }
    };

    function validSignIn(uname, pwd) {
        var valid = false;
        for (var i = 0; i < users.length; i++) {
            if (users[i].username == uname && users[i].password == pwd) {
                validid = users[i].uid;
                valid =  true;
                break;
            } 
        }
        return valid;
    }
    
});
