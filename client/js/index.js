var app = angular.module('myApp', []);
app.controller('Controler', function($scope, $http) {
    // Defind each API or url
    var userAPI = 'http://nsp1010.herokuapp.com/users/';
    var weatherAPI = 'http://nsp1010.herokuapp.com/weathers/';
    var picAPI = 'http://openweathermap.org/img/w/';
    
    // Search weather info from restful api by get method 
    document.getElementById('search').onclick = function() {
        var city = document.getElementById('city').value;
        var url = weatherAPI + city;
        $http.get(url)
        .then(function(response){
            $scope.temp = "Temp: " + response.data.temp + "°C";
            $scope.min = "Min Temp:" + response.data.min + "°C";
            $scope.max = "Max Temp:" + response.data.max + "°C";
            $scope.main = "Main: " + response.data.main;
            $scope.pic = picAPI + response.data.pic + ".png";
            $scope.pre = "Pressure: " + response.data.pre;
            $scope.hum = "Humidity: " + response.data.hum;
        });
    };
    
    // Add a city to user's favour list if the user is signed in
    document.getElementById('add').onclick = function() {
        if(sessionStorage.getItem("username") != null){
            var city = document.getElementById('city').value;
            var newFavour = [city];
            var userId = sessionStorage.getItem("uid");
            $http.post(userAPI + userId, newFavour).then(function(response) {
                alert(response.data.message);
            });
            // Refresh the page to reload the favour list
            window.location.reload(); 
        } else {
            alert("You must sign in before using it!");
        }
    };

    // Delete all the data of favour list if the user is signed in
    document.getElementById('delete').onclick = function() {
        if(sessionStorage.getItem("username") != null){
            var userId = sessionStorage.getItem("uid");
            $http.delete(userAPI + userId).then(function(response) {
                alert(response.data.message);
                window.location.reload(); 
            });
        } else {
            alert("You must sign in before using it!");
        }
    };
    
    if(sessionStorage.getItem("uid") != null){
        // If the user is signed in, then show sign out 
        document.getElementById('signin').style.display="none";
        document.getElementById('signout').style.display="block";
        showFavour(sessionStorage.getItem("uid"));
        // showUserInfo(sessionStorage.getItem("uid"));
        $http.get(userAPI + sessionStorage.getItem("uid"))
        .then(function(response){
            $scope.username = response.data.username;
            $scope.name = response.data.name;
            $scope.email = response.data.email;
            $scope.password = response.data.password;
            console.log(response.data);
        });
    } else {
        // If the user is signed out, then show sign in 
        document.getElementById('signin').style.display="block";
        document.getElementById('signout').style.display="none";
    }
    
    // The function to get and show favour list of user
    function showFavour(userId){
        $http.get(userAPI + userId)
        .then(function(response){
            $scope.favours = response.data.favour;
        });
    }
    
    // The function to get and show all information of user
    function showUserInfo(userId){
        $http.get(userAPI + userId)
        .then(function(response){
            $scope.username = response.data.username;
            $scope.name = response.data.name;
            $scope.email = response.data.email;
            $scope.password = response.data.password;
            console.log(response.data);
        });
    }
});
